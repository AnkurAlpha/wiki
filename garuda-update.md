---
title: Garuda Update
description: Garuda-update, the update script of Garuda Linux
published: true
date: 2024-01-14T03:56:11.338Z
tags: cheatsheet
editor: markdown
dateCreated: 2022-05-04T00:00:20.221Z
---

`garuda-update` is Garuda Linux's preferred update solution. The update script allows Garuda Linux to apply certain hotfixes and update automations for a smoother experience.

### What does Garuda Update do?
1. A self-update is performed to ensure the latest fixes are downloaded.
2. The mirrorlist is automatically refreshed using `rate-mirrors`
3. The archlinux and chaotic keyrings are updated
4. The `garuda-hotfixes` package is updated before any other system packages are updated to fix any important issues.
5. Package related hotfixes such as conflicts and other issues are automatically resolved after they have been approved by the developers.
6. The system update is applied. Common pacman prompts are automatically answered by "auto-pacman". The user is asked to answer with "y" if they want to apply the update.
7. Common post-update tasks are executed. The mlocate database is updated, fish completions and micro plugins are updated.
8. If needed, a changelog showing major configuration changes is shown.

### Usage

Simply use `garuda-update` or `update` in your shell of choice. Both commands do the same thing. Additionally, the following parameters are available:

|Parameter         |Environment variable|Description                              |
|------------------|--------------------|-----------------------------------------|
|--skip-mirrorlist |SKIP_MIRRORLIST=1   |Don't update the mirrorlist before applying the system update.|
|--aur/-a          |UPDATE_AUR=1        |Update AUR packages via paru/yay after system update.|
|--noconfirm       |PACMAN_NOCONFIRM=1  |Emulate a custom (safer) version of --noconfirm for pacman|
|-- [extra pacman parameters] |PACMAN_EXTRA_OPTS|Add extra pacman parameters (everything after the two dashes)|

#### Config file

A config file can be created at `/etc/garuda/garuda-update/config` that can contain the environment variables in the following format:
```
SKIP_MIRRORLIST=1
UPDATE_AUR=1
```

### Extended Garuda Update Features

Garuda-update offers additional features beyond the basic update process. These features provide users with more control and flexibility over system maintenance.

| Command                              | Description                                                                                                          |
|--------------------------------------|----------------------------------------------------------------------------------------------------------------------|
| `garuda-update remote fix`           | Reset `pacman.conf` and refresh keyrings.                                                                            |
| `garuda-update remote keyring`       | Refresh keyring without resetting `pacman.conf`.                                                                     |
| `garuda-update remote fullfix`       | Reset `pacman.conf`, refresh keyrings, and reinstall all packages in your OS.                                        |
| `garuda-update remote reinstall`     | Reinstall all packages in your OS.                                                                                   |
| `garuda-update remote reset-snapper` | Remove all backup subvolumes and snapshots created by Snapper, and force recreates Snapper configs.                  |
| `garuda-update remote reset-audio`   | Reinstall Pipewire-support and WirePlumber, and disable/enable their services to reset the audio configuration.      |

> **Be cautious with the remote commands!** Some operations, like resetting Snapper or audio configurations, may have significant impacts on your system. 
{.is-warning}
