---
title: Garuda Linux Sistem Gereksinimleri
description: Garuda Linux'un düzgün çalışabilmesi için cihazınızın sahip olması gereken özellikler
published: true
date: 2022-02-18T15:10:45.351Z
tags: 
editor: markdown
dateCreated: 2022-02-18T15:10:45.351Z
---

Donanım gereksinimleri ilk bakışta fazla görünüyor olabilir, ancak bu çoğunlukla [Calamares](https://calamares.io/about){:target="_blank"}'in kurulum sırasında çok fazla hafıza kullanması ve [bazı diğer performans iyileştirme uygulamalarının ](https://forum.garudalinux.org/t/how-does-the-distro-exactly-improve-performance/1454/2?u=dr460nf1r3){:target="_blank"} kullanılmasından kaynaklı. Pencere yöneticileri için gereken özellikler [KDE](https://wiki.archlinux.org/title/KDE){:target="_blank"}'nin gereksinimlerinden daha düşüktür. **Oyuncu sürümü en az 8GB boyutunda bir USB sürücüsü** gerektirirken **standart sürümler 4GB boyutunda bir USB sürücüsü** gerektirir.

## Gereksinimler (dr460nized)
- En Düşük: 30GB disk alanı ve 4GB RAM
- Önerilen: 40GB disk alanı ve 8GB RAM

## Önemli noktalar
- Garuda Linux fiziksel cihazlarda çalıştırılmak için optimize edildiğinden **sanal makinelerde çalıştırmanızı önermiyoruz.** Ayrıca [Picom](https://wiki.archlinux.org/title/picom){:target="_blank"} ve  [Alacritty](https://github.com/alacritty/alacritty){:target="_blank"} gibi bazı şeyler Virtualbox üzerinde doğru şekilde yapılandırılmadığı sürece çalışmayı reddediyor. Bu sebeple, **bu kullanım senaryosu için destek sağlamayacağız.⚠️** 
- Eğer **Windows ile çoklu ön yükleme** yapmayı planlıyorsanız öncelikle bu [Arch wiki yazısını](https://wiki.archlinux.org/index.php/Dual_boot_with_Windows){:target="_blank"} okumanızı öneririz çünkü aklınıza takılabilecek çoğu sorulara cevap bulabilirsiniz. Windows ile çoklu ön yükleme yapmak mümkündür ancak Windows Garuda'nın ön yükleyicisinin ([GRUB](https://wiki.archlinux.org/title/GRUB){:target="_blank"}) üzerine kendi önyükleyicisini yazabilir - bu da ön yükleyicinin düzeltilmesini gerektirir. **Eğer kendiniz yapabileceğinize inanmıyorsanız çoklu önyüklmeye yapmayın⚠️**
- Sistem boştayken kullanılan RAM miktarı diğer dağıtımlara göre daha yüksek olacaktır çünkü genel performansı arttırmak için ZRAM ve bazı diğer ayarlamaları kullanıyoruz. **[Kullanılmayan RAM boşa gider.](https://linuxatemyram.com){:target="_blank"}** Bu kadar basit!
- ISO disk görüntümüzü USB sürücünüze yazdırmak için [Garuda Downloader](https://forum.garudalinux.org/t/garuda-downloader-a-zsync-enabled-delta-downloader-for-garuda-linux-iso-files/6224){:target="_blank"} uygulamamızı, **dd(Etcher, Imagewriter,...)** ya da [Ventoy](https://www.ventoy.net/){:target="_blank"} kullanın.
- Eğer bir **NVIDIA** ekran kartı kullanıyorsanız **Wayland (Wayfire ve Sway)** kullanmaya çalışmayın! ([hepimiz Linus'un ne dediğini biliyoruz](https://piped.kavin.rocks/watch?v=_36yNWw_07g){:target="_blank"})⚠️
-Eski **NVIDIA** ekran kartları için **nvidia-390xx** sürücüsünü kullanın.