---
title: Hoşgeldiniz - Garuda Linux Wiki
description: Garuda Linux için temel açıklamalar
published: false
date: 2022-02-18T15:11:57.954Z
tags: 
editor: markdown
dateCreated: 2022-02-13T14:08:09.860Z
---

**Garuda Linux Wiki'ye hoşgeldiniz!👋🏻**

Bu, Garuda'ya özgü bilgileri içeren, [Arch Wiki](https://wiki.archlinux.org)'ye bir ekleme niteliğinde olan Wiki sayfamızdır. Bu depoda bulunmayan her konu çoğunlukla Arch wiki'de bulunuyor - bu tür yazılara olan bağlantılar çoğu sayfalarda bulunmakta😊

# Kurulum Öncesi
- [Garuda Linux sistem gereksinimleri](https://wiki.garudalinux.org/tr/system-requirements)
- [Diğer popüler Linux dağıtımları ve Garuda Linux'un karşılaştırılması]()

# Kurulum
- [Kurulum]() 
- [İlk Adımlar]()
- [Özelleştirme]()
- [Kullanışlı notlar / klavye kısayolları]()
- [Garuda Linux'ta oyun oynamak]()

# Sistem Bakımı
- [Sistemi eski durumuna geri getirme]()
- [Önyükleme sorunlarını giderme]()
- [Kısmi / yarım kalmış güncelleştirmeler]()
- [Destek hakkında]()

# Topluluktan Yardımlar
- [Forumda "Sık Sorualn Sorular ve Rehberler" kategorisi](https://forum.garudalinux.org/c/faq-and-tutorials/36){:target="_blank"}
- [Anbox](https://forum.garudalinux.org/t/ultimate-guide-to-install-anbox-in-any-arch-based-distro-especially-garuda/7453){:target="_blank"} ve [Waydroid](https://forum.garudalinux.org/t/ultimate-guide-to-install-waydroid-in-any-arch-based-distro-especially-garuda/15902){:target="_blank"}'i kurma
- [Heroic Launcher kullanarak GTA 5 kurulumu](https://forum.garudalinux.org/t/the-ultimate-guide-to-install-epic-games-gta-5-using-heroic-launcher-in-any-linux-distro/7150){:target="_blank"}
- [Root şifresi kullanarak Octopi'yi kullanmak](https://forum.garudalinux.org/t/guide-security-how-to-use-octopi-with-root-password/13307){:target="_blank"}