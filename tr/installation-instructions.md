---
title: Kurulum Kılavuzu
description: Önerilen kurulum prosedürünün açıklaması
published: true
date: 2022-03-21T15:01:35.225Z
tags: 
editor: markdown
dateCreated: 2022-02-18T15:33:13.489Z
---

Bu kılavuzda Garuda Linux "Harpy Eagle"ı nasıl kuracağınız anlatılmaktadır. Daha önce yaptıysanız da hala burada kalmak isteyebilirsizin çünkü yeni bir şeyler öğrenebilirsiniz😁

## Başlamadan önce
 - Garuda'yı bir ***MacBook Pro***'ya yüklemeyi öğrenmek için, [buraya gidebilirsiniz.](https://wiki.garudalinux.org/en/installation-instructions-macbook-pro)
 - ***Donanımınızı kontrol edin.***  Garuda'nın sistem gereksinimlerine bakmak için [bu wiki sayfasını](https://wiki.garudalinux.org/tr/system-requirements) ziyaret edebilirsiniz(bilgisayarınızın patlamayacağından eminseniz bakmanıza gerek yok)
 - Eğer **Windows ile çoklu önyükleme** yapmayı istiyorsanız, lütfen, **yapmayın⚠️**(gerçekten mi? bir de linuxoid olduğunuzu mu düşünüyorsunuz?) 
 - Yine de yapmak mı istiyorsunuz? [Windows2un yanına kurmak için resmi olmayan rehber burada](https://forum.garudalinux.org/t/tutorial-for-dual-boot-garuda-linux-windows-10/3884)(eskimiş olabilir).
 
 - **İstediğiniz versiyonu indirin**. Ayrıca dosyanın checksumlarını da kontrol edin, bu şekilde dosyanın düzgün bir şekilde indirildiğinden emin olabilirsiniz. Bunu yapmak için resmi ve önerilen yöntem [Garuda Downloader](https://forum.garudalinux.org/t/garuda-downloader-a-zsync-enabled-delta-downloader-for-garuda-linux-iso-files/6224) kullanmak; indirme işine bakar(kim beklerdi ki?) ve indirilen disk görüntüsünü USB sürücünüze görsel arayüzler kullanarak yazdırır. Zaten var olan bir disk görünütüsünü tohum olarak sağlamak da isteyebilirsiniz - uygulama kullanılan bant genişliğini minimize etmek için Zsync2 kullanır, ve bu da sadece dosyanın farklı kısımlarını indirir.
![garuda-downloader-main.png](/screenshots/garuda-downloader-main.png)
Bir disk görüntüsünü seçip "indir"e basıyoruz.
![garuda-downloader-done.png](/screenshots/garuda-downloader-done.png)
.."Flasher"a basın.
![garuda-downloader-flasher.png](/screenshots/garuda-downloader-flasher.png)
..ve sihirbazdan devam edin
 - Bittiği zaman BIOS ayarlarınızdan önyükleme için az önce kullandığınız sürücüyü seçin.
 - Bunu nasıl yapacağınız hakkındaki bilgiler [burada](https://searx.garudalinux.org/search?q=BIOS%20menu%20key%20brand&categories=general) olabilir, işlem cihaz üreticinize bağlı.
 
## Kurulum
 - Kurulum medyasını yüklediniz. **Calamares**i çalıştırmak için masaüstündeki "Kur(Install)" butonuna basın. Eğer bir laptop kullanıyorsanız şarj aletinizi de bağlayın. İnternet bağlantısı zorunlu değil.
 ![calamares.webp](/screenshots/calamares.webp)
Açtğınızda 8 adımdan oluştuğunu göreceksiniz.
 - **HOŞGELDİNİZ** - Burada istedğiniz dili seçiyorsunuz.
 - **KONUM** - Burada bir harita göreceksiniz. Eğer coğrafya biliyorsanız yaşadığınız yere tıklayın. Bu cihazın **zaman dilimi**ni ayarlar.
 - **KLAVYE** - Burada klavye düzeninizi seçiyorsunuz.
 - **DİSK BÖLÜMLERİ** - Eğer umurunuzda değilse **Diski sil**e basabilirsiniz. Bu Garuda'yı ana işletim sisteminiz olarak kullanmak için önerilen yöntemdir. Eğer umurunuzdaysa **Elle bölümlendirme**yi seçin. Bölümlendirme kılavuzunu biraz sonra bulabilirsiniz.
  - **KULLANICILAR** - Burada kullanıcı adınızı, alan adınızı ve şifrenizi belirliyorsunuz.
  - **ÖZET** - Burada önceki adımlarda seçtiklerinizin bir özeti gösterilir (konum ve dil gibi)
  - **KURULUM** - Garuda Linux'un kurulumu. Bir göstergenin kurulum sonunda mutlu olduğunu görebilirsiniz.
  - **SON** - Kurulum başarılı mesajı çıktğında, bilgisayarınızı yeniden başlatın.
  
**İŞİNİZE YARAYABİLİR:**
- [Arch Linux wikisinde disk bölümlendirmeyle ilgili bir sayfa](https://wiki.archlinux.org/title/Partitioning)
- [Arch Linux wikisinde Windows'un yanına kurma hakkındaki sayfa](https://wiki.archlinux.org/title/Dual_boot_with_Windows) (lütfen, yapmayın)

## Kurulum Sonrası
Tamam, Garuda Linux'u kurdunuz, değil mi? Tebrikler! Sizinle gurur duyuyorum.
Hadi kurulum sonrasında yapılacak şeylere bakalım.

### Kurulum yardımcısı - otomatik olarak adımlardan geçmek
Kurulum yardımcısı ilk açışta çıkacaktır. Kurulum sonrasında mutlaka yapılması gereken adımlardan geçeceksiniz ve ayrıca birçok Linux yazılımını içeren bir sihirbazı da açma fırsatı bulacaksınız. İlk önce sistemi güncellemek isteyip istemediğiniz sorulacak, ki bunu yapmanızı oldukça öneririz. Bu güncelleştirmeyi başlatacak, yansı listelerini en iyi indirme hızı için güncelleyecek, birkaç bakım işlemi yapacak ve en önemlisi: [garuda-update](https://gitlab.com/garuda-linux/packages/stable-pkgbuilds/garuda-update/-/blob/master/garuda-update) scriptini kullnarak en sonunda yapılması gereken müdahalelerde bulunacak. Bunu zaman zaman bazı sorunlu durumlardan kurtulmak için kullanıyoruz, Arch'ın geçenlerde depolarına Python 3.10'u ekledikten sonra [bütün paketlerin yeniden yüklenmesinin gerekmesi gibi.](https://forum.garudalinux.org/t/intervention-potentially-required-python-3-10-and-you/15342)
![setup-assistant-update.png](/screenshots/setup-assistant-update.png)
Bundan sonra uygulama sihirbazını çalıştırıp gerekli yazılımları yükleyebiliriz - yazıcı desteği de bunlardan biri.
![setup-assistant-software.png](/screenshots/setup-assistant-software.png)
Çoğu uygulamanın yanında bu paketin ne yaptığıyla ilgili kısa bir açıklama bulunuyor. Bu hangisini seçmeniz konusunda yardımcı olacaktır.
![setup-assistant-selection.png](/screenshots/setup-assistant-selection.png)
En sonunda yardımcı bir terminal penceresi açacak(bu noktada şifrenizi girmeniz gerekebilir) ve seçtiğiniz yazılımları yükleyecek. Ekstra kurulum (systemd hizmetlerini etkinleştirmek gibi)
işlemleri otomatik olarak yapılacaktır.

### Yansı listelerini elle yenilemek
![garuda-assistant-refresh.png](/screenshots/garuda-assistant-refresh.png)
 - Garuda Asistanda "Refresh mirrorlist"e tıklayın. **reflector-simple**'ı, [reflector](https://wiki.archlinux.org/title/Reflector) için grafiksel arayüzü, açacaktır.
 - **reflector-simple**da ülkenizi ve isterseniz dünya çapındaki yansıları seçin, ve sonra **"OK"** e basın
 - Pencere kapanacak ama işlem henüz bitmedi. Yeni bir pencere açılana kadar bekleyin ve kaydete basın
 - Terminali açın. Terminal olarak kullanılan uygulama versiyona göre değişebilir. Ancak bütün versiyonlar [Alacritty](https://wiki.archlinux.org/title/Alacritty) ile gelir - bence en iyilerin en iyisi
 - Sonra, `sudo pacman -Syyuu` yazın (ve istemesi halinde şifrenizi girin) ve bitene kadar bekleyin. Herhangi bir sorun çıkarsa, [forumlara](https://wiki.garudalinux.org/en/forum.garudalinux.org) veya [Telegram sohbetimize](https://wiki.garudalinux.org/en/t.me/garudalinux) destek için gelin.
 
 ### Sonuç
 Sistemi tamamen kurmak için yapmamız gerekenler bu kadar. Elbette **Octopi**, **Garuda Gamer** gibi şeyleri de öğrenmek isteyebilirsiniz. Atılacak diğer adımlar hakkındaki wiki yazımıza [buradan](https://wiki.garudalinux.org/en/post-installation-tasks) ulaşabilirsiniz. Bu yazıyı okumanızı öneriyoruz çünkü yeni bir kurulumdan sonra çoğu kişinin yapmak isteyebileceği şeyleri anlatıyor.
Ve şimdi, görüşürüz! **İyi bir linuxdeneyimi diliyoruz**😌
Not: Eğer Garuda Linux'u ilk defa kurduysanız sizinle gerçekten gurur duyuyorum😇