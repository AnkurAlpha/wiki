---
title: Switching between integrated and dedicated graphics
description: How to take advantage of your dedicated NVIDIA/AMD GPU
published: true
date: 2022-06-13T23:01:27.265Z
tags: guide, nvidia, optimus
editor: markdown
dateCreated: 2022-06-13T22:19:00.082Z
---

## Introduction
If your device (usually laptop) features 2 graphics cards, Garuda Linux by default only uses your integrated GPU to save power. This article will elaborate on how to "switch" to your dedicated GPU for running GPU intensive applications such as games.

## NVIDIA PRIME Render Offload
If you have an NVIDIA dedicated GPU with the proprietary NVIDIA drivers, you can start an application using NVIDIA's official "PRIME Render Offload" technology by prepending the "prime-run" command.
Example: `prime-run glxgears`

You can verify PRIME Render Offload is working correctly by using the following command:
`prime-run glxinfo -B | grep "OpenGL vendor string"`
Your output should look similar to this:
>OpenGL vendor string: NVIDIA Corporation

## PRIME
If you have an NVIDIA GPU without the proprietary driver installed or have an AMD GPU, you can you can start an application using PRIME by prepending the "DRI_PRIME=1" environment variable.
Example: `DRI_PRIME=1 glxgears`

You can verify PRIME is working correctly by using the following command:
`DRI_PRIME=1 glxinfo -B | grep "OpenGL vendor string"`
Your output should look similar to this:
>OpenGL vendor string: AMD

## Common misconceptions and antipatterns
Quite often, software such as "optimus-manager" or "optimus-switch" is used instead of PRIME. Although they feature "hybrid mode", they also feature modes that exclusively switch to the dedicated or iGPU. Using these modes is an usually not recommended, because
1. If your NVIDIA driver ever fails to load, your system will be unable to enter a graphical session
2. In NVIDIA mode, the dedicated GPU is always used and therefore always turned on even if no software that could properly take advantage of it is running. The iGPU is much better suited for running low power common desktop applications.
3. Switching modes requires logging out and logging back in or otherwise restarting X11. Meanwhile PRIME can be used without restarting X11.

## Steam games
To launch Steam games on your dedicated GPU, add the following to your game start parameters:

#### NVIDIA PRIME Render Offload
`prime-run %command%`
#### PRIME
`DRI_PRIME=1 %command%`

Other launch parameters can be appended to the end like so:
Example: `prime-run %command% --fullscreen`